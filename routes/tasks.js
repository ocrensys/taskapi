const express = require("express");
const { db } = require("../firebase");
const router = express.Router();
const TASKS = "tasks";

/* GET task listing. */
router.get("/", async (req, res, next) => {
  const querySnapshot = await db.collection(TASKS).get();
  const tasks = querySnapshot.docs.map((doc) => ({
    id: doc.id,
    ...doc.data(),
  }));

  res.send(tasks);
});

/* POST create task. */
router.post("/", async (req, res) => {
  try {
    const { title, description } = req.body;
    const result = await db.collection(TASKS).add({
      title,
      description,
      createdAt: new Date().getTime(),
      updatedAt: new Date().getTime(),
    });

    res.send(result);
  } catch (error) {
    res.send(error);
  }
});

/* PUT update task. */
router.put("/:id", async (req, res, next) => {
  try {
    const { title, description, isCompleted } = req.body;
    const { id } = req.params;
    const result = await db.collection(TASKS).doc(id).update({
      title,
      description,
      isCompleted,
      updatedAt: new Date().getTime(),
    });
    res.send(result);
  } catch (error) {
    res.send(error);
  }
});

/* DELETE delete task. */
router.delete("/:id", async (req, res, next) => {
  try {
    const result = await db.collection(TASKS).doc(req.params.id).delete();
    res.send(result);
  } catch (error) {
    res.send(error);
  }
});

module.exports = router;
